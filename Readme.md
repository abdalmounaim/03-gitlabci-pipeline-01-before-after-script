# GitLab CI: Pipeline Syntax: SCRIPT, BEFORE_SCRIPT  and  AFTER_SCRIPT
([Source](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/))

---

## SCRIPT
La déclaration script est donc la seule obligatoire dans un job. Cette déclaration est le coeur du job car c'est ici que vous indiquerez les actions à effectuer.

Il peut appeler un ou plusieurs script(s) de votre projet, voire exécuter une ou plusieurs ligne(s) de commande.

``` yaml
job:script:
  script: ./bin/script/my-script.sh ## Appel d'un script de votre projet

job:scripts:
  script: ## Appel de deux scripts de votre projet
    - ./bin/script/my-script-1.sh
    - ./bin/script/my-script-2.sh

job:command:
  script: printenv # Exécution d'une commande

job:commands:
  script: # Exécution de deux commandes
    - printenv
    - echo $USER
``` 

## BEFORE_SCRIPT ET AFTER_SCRIPT

Ces déclarations permettront d'exécuter des actions avant et aprés votre script principal. Ceci peut être intéressant pour bien diviser les actions à faire lors des jobs, ou bien appeler ou exécuter une action avant et aprés chaque job

``` yaml
before_script: # Exécution d'une commande avant chaque `job`
  - echo 'start jobs'

after_script: # Exécution d'une commande aprés chaque `job`
  - echo 'end jobs'

job:no_overwrite: # Ici le job exécutera les action du `before_script` et `after_script` par défaut
  script:
    - echo 'script'

job:overwrite:before_script:
  before_script:
    - echo 'overwrite' # N'exécutera pas l'action définie dans le `before_script` par défaut
 script:
    - echo 'script'

job:overwrite:after_script:
 script:
    - echo 'script'
  after_script:
    - echo 'overwrite' # N'exécutera pas l'action définie dans le `after_script` par défaut

job:overwrite:all:
  before_script:
    - echo 'overwrite' # N'exécutera pas léaction définie dans le`before_script` par défaut
 script:
    - echo 'script'
  after_script:
    - echo 'overwrite' # N'exécutera pas l'action définie dans le `after_script` par défaut
``` 